# output "dev-vpc-id" {
#   value = aws_vpc.development-vpc.id
# }

# output "dev-subnet1-id" {
#   value = aws_subnet.dev-subnet-1.id
# }

output "ec2-public_ip" {
  value = module.myapp-server.instance.public_ip
}

resource "aws_default_security_group" "default-sg" {
  vpc_id = var.vpc_id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "amznLinux" {
    most_recent = true
    filter {
      name = "name"
      values = [var.image_name]
    }
  owners = ["137112412989"] ### Owner: Amazon
}

resource "aws_instance" "myapp-ec2" {
  ami = data.aws_ami.amznLinux.id
  instance_type = var.instance_type

  availability_zone = var.avail_zone
  subnet_id = var.subnet_id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  
  associate_public_ip_address = true
  #key_name = ###ADD key name  
  
  tags = {
      Name = "${var.env_prefix}-server"
  }
}
